package frc.simulation;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.subsystems.Swerve.ModuleConstants;
import frc.robot.subsystems.Swerve.SwerveModule;

public class ModuleSim extends SwerveModule {


  private double currentSteerPositionRad = 0;
  private double currentDrivePositionMeters = 0;
  private double currentSpeed = 0;

  public ModuleSim(ModuleConstants moduleConstants, ShuffleboardTab swerveTab) {
    super(moduleConstants, swerveTab);

  }

  //all sim special functions
  @Override
  public SwerveModuleState getState(){
    return new SwerveModuleState(currentSpeed , new Rotation2d(currentSteerPositionRad));
  }

  @Override
  public void setDesierdState(SwerveModuleState desiredState, boolean isOpenLoop) {
      if (Math.abs(desiredState.speedMetersPerSecond) < 0.001) {
          currentSpeed = 0;
          return;
      }
      // Optimize the reference state to avoid spinning further than 90 degrees
      desiredState = SwerveModuleState.optimize(desiredState, new Rotation2d(currentSteerPositionRad));

      currentSpeed = desiredState.speedMetersPerSecond;
      currentSteerPositionRad = desiredState.angle.getRadians();
  }

  public Rotation2d getAngle() {
    return new Rotation2d(currentSteerPositionRad);
}

  @Override
  public SwerveModulePosition getPosition() {
      return new SwerveModulePosition(
              currentDrivePositionMeters,
              new Rotation2d(currentSteerPositionRad)
      );
  }

  @Override
  public SwerveModuleState getDesierdState() {
      return getState();
  }

  //sim periodic for modules
  @Override
  public void periodic() {
    currentDrivePositionMeters += currentSpeed * Constants.LoopTime;

  }
}
