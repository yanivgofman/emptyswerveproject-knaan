package frc.simulation;

import edu.wpi.first.hal.SimDevice;
import edu.wpi.first.hal.SimDouble;
import edu.wpi.first.hal.SimDevice.Direction;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.wpilibj.RobotBase;
import edu.wpi.first.wpilibj2.command.Subsystem;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.subsystems.Swerve.SwerveSubsystem;


public class SwerveGyrocopeSim{

    private final static double dtSeconds = 0.020;

    private final SimDouble simYawDegrees;


    public SwerveGyrocopeSim() {

        SimDevice simDevice = SimDevice.create("Gyro", 0);
        if(RobotBase.isSimulation())
            this.simYawDegrees = simDevice.createDouble("Yaw", Direction.kBidir, 0.0);
        else
            this.simYawDegrees = new SimDouble(0);
        }

    public double getYawDegrees() {
        return this.simYawDegrees.get();
    }

    public void updateGyroscope(){
        SwerveModuleState[] swerveDriveWheelStates =
            new SwerveModuleState[]{
                SwerveSubsystem.fakeModules[0].getState(),
                SwerveSubsystem.fakeModules[1].getState(),
                SwerveSubsystem.fakeModules[2].getState(),
                SwerveSubsystem.fakeModules[3].getState()
            };

        ChassisSpeeds chassisSpeeds = Constants.Swerve.Kinematics.toChassisSpeeds(swerveDriveWheelStates);
        //chassisSpeeds = ChassisSpeeds.fromDiscreteSpeeds(chassisSpeeds, dtSeconds);
        double omegaRadiansPerSecond = chassisSpeeds.omegaRadiansPerSecond;
        double omegaDegreesPerSecond = Math.toDegrees(omegaRadiansPerSecond);
        double dOmega = omegaDegreesPerSecond * dtSeconds;

        double simYawDegrees = getYawDegrees();
        simYawDegrees += dOmega;
        simYawDegrees %= 360.0;
        simYawDegrees = simYawDegrees < 0 ? simYawDegrees + 360.0 : simYawDegrees;
        this.simYawDegrees.set(simYawDegrees);
    }

    // @Override
    // public void periodic() {
    //     updateGyroscope();
    // }
}
