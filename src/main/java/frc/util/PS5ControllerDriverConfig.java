package frc.util;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.networktables.GenericEntry;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.RobotBase;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.RepeatCommand;
import frc.robot.Constants;
import frc.robot.Constants.Elivator;
import frc.robot.commands.SetFormationX;
import frc.robot.subsystems.Swerve.SwerveSubsystem;
import frc.util.PS5Controller.PS5Axis;
import frc.util.PS5Controller.PS5Button;
import frc.util.MathUtils;



/**
 * Driver controls for the PS5 controller
 */
public class PS5ControllerDriverConfig {

    // Some of these are not currently used, but we might want them later
    @SuppressWarnings("unused")
    private double translationalSensitivity = Constants.OIConstants.TRANSLATIONAL_SENSITIVITY;
    @SuppressWarnings("unused")
    private double translationalExpo = Constants.OIConstants.TRANSLATIONAL_EXPO;
    @SuppressWarnings("unused")
    private double translationalDeadband = Constants.OIConstants.TRANSLATIONAL_DEADBAND;
    private double translationalSlewrate = Constants.OIConstants.TRANSLATIONAL_SLEWRATE;

    @SuppressWarnings("unused")
    private double rotationSensitivity = Constants.OIConstants.ROTATION_SENSITIVITY;
    @SuppressWarnings("unused")
    private double rotationExpo = Constants.OIConstants.ROTATION_EXPO;
    @SuppressWarnings("unused")
    private double rotationDeadband = Constants.OIConstants.ROTATION_DEADBAND;
    private double rotationSlewrate = Constants.OIConstants.ROTATION_SLEWRATE;

    private double headingSensitivity = Constants.OIConstants.HEADING_SENSITIVITY;
    private double headingExpo = Constants.OIConstants.HEADING_EXPO;
    private double headingDeadband = Constants.OIConstants.HEADING_DEADBAND;
    private double previousHeading = 0;

    private final DynamicSlewRateLimiter rotLimiter = new DynamicSlewRateLimiter(rotationSlewrate);
    private final DynamicSlewRateLimiter headingLimiter = new DynamicSlewRateLimiter(headingSensitivity);



    private final PS5Controller kDriver = new PS5Controller(0);
    private final SwerveSubsystem drive;
    private final ShuffleboardTab controllerTab;
    private final boolean shuffleboardUpdates;

    private GenericEntry translationalSensitivityEntry, translationalExpoEntry, translationalDeadbandEntry, translationalSlewrateEntry;
    private GenericEntry rotationSensitivityEntry, rotationExpoEntry, rotationDeadbandEntry, rotationSlewrateEntry;
    private GenericEntry headingSensitivityEntry, headingExpoEntry, headingDeadbandEntry;


    public PS5ControllerDriverConfig(
        SwerveSubsystem drive, ShuffleboardTab controllerTab, 
        boolean shuffleboardUpdates) {

        this.drive = drive;
        this.controllerTab = controllerTab;
        this.shuffleboardUpdates = shuffleboardUpdates;
    }

    public void configureControls() {
        if(RobotBase.isReal()){
        // reset the yaw forward. Mainly useful for testing/driver practice
        kDriver.get(PS5Button.OPTIONS).onTrue(new InstantCommand(() -> drive.setYaw(
                new Rotation2d(DriverStation.getAlliance() == Alliance.Red ? 0 : Math.PI)
                                                                                                   )));

        // reset the yaw backward. Mainly useful for testing/driver practice
        kDriver.get(PS5Button.CREATE).onTrue(new InstantCommand(() -> drive.setYaw(
                new Rotation2d(DriverStation.getAlliance() == Alliance.Blue ? 0 : Math.PI)
                                                                                                  )));

        // set the wheels to X
        kDriver.get(PS5Button.SQUARE).whileTrue(new RepeatCommand(new SetFormationX(drive)));


        // Resets the modules to absolute if they are having the unresolved zeroing error
        kDriver.get(PS5Button.CROSS).onTrue(new InstantCommand(() -> drive.resetModulesToAbsolute()));

    }
        if(RobotBase.isSimulation()){
            // reset the yaw forward. Mainly useful for testing/driver practice
            kDriver.get(PS5Button.OPTIONS).onTrue(new InstantCommand(() -> drive.setYaw(
                new Rotation2d(
                    DriverStation.getAlliance() == Alliance.Red ? -Math.PI/2 : Math.PI/2
                    ))));

            // reset the yaw backward. Mainly useful for testing/driver practice
            kDriver.get(PS5Button.CREATE).onTrue(new InstantCommand(() -> drive.setYaw(
                new Rotation2d(DriverStation.getAlliance() == Alliance.Blue ? -Math.PI/2 : Math.PI/2)
                                                                                                )));

            // set the wheels to X
            kDriver.get(PS5Button.SQUARE).whileTrue(new RepeatCommand(new SetFormationX(drive)));

            // Resets the modules to absolute if they are having the unresolved zeroing error
            kDriver.get(PS5Button.CROSS).onTrue(new InstantCommand(() -> drive.resetModulesToAbsolute()));
               
        }
    }

    public void setupShuffleboard() {
        if (!shuffleboardUpdates) return;

        translationalSensitivityEntry = controllerTab.add("translationalSensitivity", Constants.OIConstants.TRANSLATIONAL_SENSITIVITY).getEntry();
        translationalExpoEntry = controllerTab.add("translationalExpo", Constants.OIConstants.TRANSLATIONAL_EXPO).getEntry();
        translationalDeadbandEntry = controllerTab.add("translationalDeadband", Constants.OIConstants.TRANSLATIONAL_DEADBAND).getEntry();
        translationalSlewrateEntry = controllerTab.add("translationalSlewrate", Constants.OIConstants.TRANSLATIONAL_SLEWRATE).getEntry();
        rotationSensitivityEntry = controllerTab.add("rotationSensitivity", Constants.OIConstants.ROTATION_SENSITIVITY).getEntry();
        rotationExpoEntry = controllerTab.add("rotationExpo", Constants.OIConstants.ROTATION_EXPO).getEntry();
        rotationDeadbandEntry = controllerTab.add("rotationDeadband", Constants.OIConstants.ROTATION_DEADBAND).getEntry();
        rotationSlewrateEntry = controllerTab.add("rotationSlewrate", Constants.OIConstants.ROTATION_SLEWRATE).getEntry();
        headingSensitivityEntry = controllerTab.add("headingSensitivity", Constants.OIConstants.HEADING_SENSITIVITY).getEntry();
        headingExpoEntry = controllerTab.add("headingExpo", Constants.OIConstants.HEADING_EXPO).getEntry();
        headingDeadbandEntry = controllerTab.add("headingDeadband", Constants.OIConstants.HEADING_DEADBAND).getEntry();
    }

    public double getRawSideTranslation() {
        if(RobotBase.isReal())
            return kDriver.get(PS5Axis.LEFT_X);
        else
            return kDriver.get(PS5Axis.LEFT_Y);

    }

    
    
    public boolean isPOVpressed(){ 
        return kDriver.get(PS5Button.TOUCHPAD).getAsBoolean();
    }

    public double getRotation() {
        return MathUtils.expoMS(MathUtil.applyDeadband(getRawRotation(), Constants.OIConstants.DEADBAND), 2) * Constants.Swerve.kMaxAngularSpeed * 1;
    }


    public double getRawForwardTranslation() {
        if(RobotBase.isReal())
            return kDriver.get(PS5Axis.LEFT_Y);
        else
            return -kDriver.get(PS5Axis.LEFT_X);
    }

    public double getRawRotation() {
        return kDriver.get(PS5Axis.RIGHT_X);
    }

    public double getRawHeadingAngle() {
        return Math.atan2(kDriver.get(PS5Axis.RIGHT_X), -kDriver.get(PS5Axis.RIGHT_Y)) - Math.PI / 2;
    }

    
    public double getRawHeadingMagnitude() {
        return MathUtils.calculateHypotenuse(kDriver.get(PS5Axis.RIGHT_X), kDriver.get(PS5Axis.RIGHT_Y));
    }

    public double getForwardTranslation() {
        return -MathUtils.expoMS(MathUtil.applyDeadband(getRawForwardTranslation(), Constants.OIConstants.DEADBAND), 2) * Constants.Swerve.kMaxSpeed * 1;
    }

    public boolean getIsPOV(){
        return isPOVpressed();
    }
    public double getSideTranslation() {
        return -MathUtils.expoMS(MathUtil.applyDeadband(getRawSideTranslation(), Constants.OIConstants.DEADBAND), 2) * Constants.Swerve.kMaxSpeed * 1;
    }


    public double getHeading() {
        if (getRawHeadingMagnitude() <= headingDeadband) return headingLimiter.calculate(previousHeading, 1e-6);
        previousHeading = headingLimiter.calculate(getRawHeadingAngle(), MathUtils.expoMS(getRawHeadingMagnitude(), headingExpo) * headingSensitivity);
        return previousHeading;
    }
    


    public boolean getIsSlowMode() {
        return kDriver.get(PS5Button.RIGHT_TRIGGER).getAsBoolean();
    }

    public boolean getIsAlign() {
        return kDriver.get(PS5Button.LEFT_TRIGGER).getAsBoolean();
    }

}
