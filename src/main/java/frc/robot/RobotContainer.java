package frc.robot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.pathplanner.lib.PathConstraints;
import com.pathplanner.lib.PathPlanner;
import com.pathplanner.lib.PathPlannerTrajectory;
import com.pathplanner.lib.auto.SwerveAutoBuilder;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.shuffleboard.EventImportance;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.Constants.Elivator.kElivator;
import frc.robot.commands.SwerveDefaultCommand;
import frc.robot.subsystems.Swerve.SwerveSubsystem;
import frc.util.PS5ControllerDriverConfig;
import frc.util.Vision;

public class RobotContainer {

  
    // Shuffleboard auto chooser
    private final SendableChooser<Command> autoCommand = new SendableChooser<>();

    //shuffleboard tabs
    // The main tab is not currently used. Delete the SuppressWarning if it is used.
    @SuppressWarnings("unused")
    private final ShuffleboardTab mainTab = Shuffleboard.getTab("Main");
    private final ShuffleboardTab drivetrainTab = Shuffleboard.getTab("Drive");
    private final ShuffleboardTab swerveModulesTab = Shuffleboard.getTab("Swerve Modules");
    private final ShuffleboardTab autoTab = Shuffleboard.getTab("Auto");
    private final ShuffleboardTab controllerTab = Shuffleboard.getTab("Controller");
    private final ShuffleboardTab visionTab = Shuffleboard.getTab("Vision");

   private final Vision vision  = new Vision(visionTab);;

    // The robot's subsystems are defined here...
    private SwerveSubsystem drive = new SwerveSubsystem(drivetrainTab, swerveModulesTab, vision);

    // Controllers are defined here
   private final PS5ControllerDriverConfig driver;


   public RobotContainer() {
      driver = new PS5ControllerDriverConfig(this.drive, controllerTab , Constants.Telemetry);
      driver.configureControls();
      vision.setupVisionShuffleboard();

      drive.setDefaultCommand(new SwerveDefaultCommand(this.drive, driver , controllerTab));
        
      // This is really annoying so it's disabled
      DriverStation.silenceJoystickConnectionWarning(true);
      LiveWindow.disableAllTelemetry(); // LiveWindow is causing periodic loop overruns
      LiveWindow.setEnabled(false);

      autoTab.add("Auto Chooser", autoCommand);

        
      if(DriverStation.getAlliance() == Alliance.Blue){
            drive.setYaw(new Rotation2d(180));
        }else{
            drive.setYaw(new Rotation2d(0));

         }


        if (Constants.Telemetry) loadCommandSchedulerShuffleboard();

        driver.setupShuffleboard();

    configureBindings();
  }

  private void configureBindings() {
    driver.configureControls();
}


  public Command getAutonomousCommand() {
    PathPlannerTrajectory path1 = 
    PathPlanner.loadPath(
        "PathTry1", 
        new PathConstraints( 
          Constants.AutoConstants.MAX_AUTO_SPEED,
          Constants.AutoConstants.MAX_AUTO_ACCEL));

    PathPlannerTrajectory path2 = 
    PathPlanner.loadPath(
        "PathTry2", 
        new PathConstraints( 
          Constants.AutoConstants.MAX_AUTO_SPEED,
          2));

    PathPlannerTrajectory path3 = 
          PathPlanner.loadPath(
              "PathTry3", 
              new PathConstraints( 
                Constants.AutoConstants.MAX_AUTO_SPEED,
                Constants.AutoConstants.MAX_AUTO_ACCEL));

    PathPlannerTrajectory path4 = 
                PathPlanner.loadPath(
                    "PathTry4", 
                    new PathConstraints( 
                      Constants.AutoConstants.MAX_AUTO_SPEED,
                      2));

    PathPlannerTrajectory path5 = 
                      PathPlanner.loadPath(
                          "PathTry5", 
                          new PathConstraints( 
                            Constants.AutoConstants.MAX_AUTO_SPEED,
                            Constants.AutoConstants.MAX_AUTO_ACCEL));


    HashMap<String, Command> eventMap = new HashMap<>();

    SwerveAutoBuilder autoBuilder = new SwerveAutoBuilder(
        drive::getPose,
        drive::resetOdometry, 
        Constants.Swerve.Kinematics,
        Constants.AutoConstants.translationController,
        Constants.AutoConstants.rotationController,
        drive::setModuleStates, 
        eventMap,
        true,
        drive);

    List<PathPlannerTrajectory> paths = new ArrayList<>();
    paths.add(path1);
    paths.add(path2);
    paths.add(path3); 
    paths.add(path4);
    paths.add(path5);

  Command finalPath = autoBuilder.fullAuto(paths);


// Add your commands in the addCommands() call, e.g.
// addCommands(new FooCommand(), new BarCommand());
    SequentialCommandGroup finalAuto = new SequentialCommandGroup(
        new InstantCommand(() -> this.drive.resetYaw()),
        new InstantCommand(() -> this.drive.resetOdometry(path1.getInitialHolonomicPose())), 
        new InstantCommand(() -> this.drive.resetModulesToAbsolute()), 
        finalPath
        
        );
    return finalAuto;
  }

  public void loadCommandSchedulerShuffleboard() {
      // Set the scheduler to log Shuffleboard events for command initialize, interrupt, finish
      CommandScheduler.getInstance().onCommandInitialize(command -> Shuffleboard.addEventMarker("Command initialized", command.getName(), EventImportance.kNormal));
      CommandScheduler.getInstance().onCommandInterrupt(command -> Shuffleboard.addEventMarker("Command interrupted", command.getName(), EventImportance.kNormal));
      CommandScheduler.getInstance().onCommandFinish(command -> Shuffleboard.addEventMarker("Command finished", command.getName(), EventImportance.kNormal));
  
    }
}
