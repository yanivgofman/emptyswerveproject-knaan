package frc.robot.subsystems.Swerve;

import java.util.ArrayList;

import org.photonvision.EstimatedRobotPose;

import com.ctre.phoenix.sensors.WPI_Pigeon2;
import com.ctre.phoenix.sensors.Pigeon2.AxisDirection;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.estimator.SwerveDrivePoseEstimator;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Pose3d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.networktables.GenericEntry;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.RobotBase;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.simulation.ModuleSim;
import frc.simulation.SwerveGyrocopeSim;
import frc.util.LogManager;
import frc.util.Vision;

public class SwerveSubsystem extends SubsystemBase {
  /*
    Modules order:
    1:  Front left
    2:  Front Right
    3:  Back Left
    4: Back Right
   */

   //telemetry variables
   private final ShuffleboardTab modulesTab;
   private final ShuffleboardTab swerveTab;


   
   //odometry pose estimator
   private SwerveDrivePoseEstimator poseEstimator;
   public SwerveModulePosition[] latestPositions;
   public SwerveModulePosition[] currentposition;

   //modules 
   // This is left intentionally public
   public final SwerveModule[] modules;
   public static SwerveModule[] fakeModules;
   private final WPI_Pigeon2 pigeon;
   private final SwerveGyrocopeSim simGyro;
   
    //Vision for visiom odometry
    private final Vision vision;

    // PID Controllers for chassis movement
    private final PIDController xController;
    private final PIDController yController;
    private final PIDController rotationController;


    // Displays the field with the robots estimated pose on it
    private final Field2d fieldDisplay;

    //Shuffleboard
    private GenericEntry
            driveVelocityEntry,
            steerVelocityEntry,
            steerAngleEntry,
            driveStaticFeedforwardEntry,
            driveVelocityFeedforwardEntry,
            steerStaticFeedforwardEntry,
            steerVelocityFeedforwardEntry,
            xPosEntry,
            yPosEntry,
            headingEntry;

    private Double[] driveVelFeedForwardSaver = new Double[4];
    private Double[] driveStaticFeedForwardSaver = new Double[4];
    private Double[] steerVelFeedForwardSaver = new Double[4];
    private Double[] steerStaticFeedForwardSaver = new Double[4];

    private SwerveModule prevModule;

    // If vision is enabled
    // Do not change this. Instead, change ENABLED in VisionConstants
    // This is used in some commands that need visi on disabled
    private boolean visionEnabled = false;
    
    private boolean isPOV = false;

    private int loggerStep = 0;
    private final SendableChooser<SwerveModule> moduleChooser = new SendableChooser<>();

  
    public SwerveSubsystem(ShuffleboardTab driveTrainShuffleBoard ,ShuffleboardTab SwerveModuleTab , Vision vision) {
      this.swerveTab = driveTrainShuffleBoard;
      this.modulesTab = SwerveModuleTab;
      this.vision = vision;

      //config pigeon
      pigeon = new WPI_Pigeon2(Constants.Swerve.Id.kPigeonId);
      pigeon.configFactoryDefault();
      pigeon.configMountPose(AxisDirection.PositiveY, AxisDirection.PositiveZ);

      if(RobotBase.isReal()){
        modules = new SwerveModule[]{
                  new SwerveModule(ModuleConstants.FRONT_LEFT, SwerveModuleTab),
                    new SwerveModule(ModuleConstants.FRONT_RIGHT, SwerveModuleTab),
                    new SwerveModule(ModuleConstants.BACK_LEFT, SwerveModuleTab),
                    new SwerveModule(ModuleConstants.BACK_RIGHT, SwerveModuleTab),
                    };
        }else{
          modules = new ModuleSim[]{
            new ModuleSim(ModuleConstants.FRONT_LEFT, SwerveModuleTab),
            new ModuleSim(ModuleConstants.FRONT_RIGHT, SwerveModuleTab),
            new ModuleSim(ModuleConstants.BACK_LEFT, SwerveModuleTab),
            new ModuleSim(ModuleConstants.BACK_RIGHT, SwerveModuleTab),
            };
      }

      prevModule = modules[0];
      modules[0].setDriveInverted(false);
      modules[1].setDriveInverted(false);
      modules[2].setDriveInverted(false);
      modules[3].setDriveInverted(false);

      fakeModules = modules;
      
      pigeon.setYaw(Constants.Swerve.kStartingHeading.getDegrees());
      poseEstimator = new SwerveDrivePoseEstimator(
                Constants.Swerve.Kinematics,
                Rotation2d.fromDegrees(pigeon.getYaw()),
                getModulePositions(),
                new Pose2d() // initial Odometry Location
      );
      
      xController = new PIDController(Constants.Swerve.kTranslationalP, 0, Constants.Swerve.kTranslationalD);
      yController = new PIDController(Constants.Swerve.kTranslationalP, 0, Constants.Swerve.kTranslationalD);
      rotationController = new PIDController(Constants.Swerve.kHeadingP, 0, Constants.Swerve.kHeadingD);
      rotationController.enableContinuousInput(-Math.PI, Math.PI);
      rotationController.setTolerance(Units.degreesToRadians(0.25), Units.degreesToRadians(0.25));

      

      fieldDisplay = new Field2d();
      fieldDisplay.setRobotPose(getPose());

      latestPositions =  new SwerveModulePosition[4];
      latestPositions[0] = new SwerveModulePosition();
      latestPositions[1] = new SwerveModulePosition();
      latestPositions[2] = new SwerveModulePosition();
      latestPositions[3] = new SwerveModulePosition();

      currentposition =  new SwerveModulePosition[4];
      currentposition[0] = new SwerveModulePosition();
      currentposition[1] = new SwerveModulePosition();
      currentposition[2] = new SwerveModulePosition();
      currentposition[3] = new SwerveModulePosition();

      //initialize sim gyro
      simGyro = new SwerveGyrocopeSim();

      setupDrivetrainShuffleboard();
      setupModulesShuffleboard();

      //resets all modules to absuloute
      Timer.delay(1.0);
      resetModulesToAbsolute();

  }
    public void enableVision(boolean enabled) {
      visionEnabled = enabled;
}


      // BELOW IS TELEMETRY STUFF

    /**
     * Sets up the shuffleboard tab for the drivetrain.
     */
    private void setupDrivetrainShuffleboard() {

      swerveTab.add("Field", fieldDisplay);
      
      if (!Constants.Telemetry) return;



      // inputs
      headingEntry = swerveTab.add("Set Heading (-pi to pi)", 0).getEntry();
      xPosEntry = swerveTab.add("Input X pos(m)", 0).getEntry();
      yPosEntry = swerveTab.add("Input Y pos(m)", 0).getEntry();

      
      // add PID controllers
      swerveTab.add("xController", getXController());
      swerveTab.add("yController", getYController());
      // swerveTab.add("Auto PID", getAutoController());
      swerveTab.add("rotationController", getRotationController());

      // add angles
      swerveTab.addNumber("Yaw (deg)", () -> getYaw().getDegrees());
      swerveTab.addNumber("estimated X", () -> poseEstimator.getEstimatedPosition().getX());
      swerveTab.addNumber("estimated Y", () -> poseEstimator.getEstimatedPosition().getY());
      swerveTab.addNumber("getPitch", () -> pigeon.getPitch());
      swerveTab.addNumber("getRoll", () -> pigeon.getRoll());
      swerveTab.addNumber("pigeon yaw", () -> pigeon.getYaw());

      swerveTab.addNumber("Gyro X", () -> getAngularRate(0));
      swerveTab.addNumber("Gyro Y", () -> getAngularRate(1));
      swerveTab.addNumber("Gyro Z", () -> getAngularRate(2));

      swerveTab.addNumber("Chassis Velocity", () -> getChassisSpeedsMagnitude());

      swerveTab.addDoubleArray("desierd states",() ->getDesierdStates());
      swerveTab.addDoubleArray("actual states",() ->getActualStates());

      swerveTab.addDoubleArray("camera pose", () -> getCameraPose());
    }


    //actual swerve states for advantage scope
    public double[] getActualStates(){
        
      double[] actualStates = {
          modules[0].getAngle().getRadians(),
          modules[0].getState().speedMetersPerSecond,
          modules[1].getAngle().getRadians(),
          modules[1].getState().speedMetersPerSecond,
          modules[2].getAngle().getRadians(),
          modules[2].getState().speedMetersPerSecond,
          modules[3].getAngle().getRadians(),
          modules[3].getState().speedMetersPerSecond
      };
      return actualStates;
    }  

    //desierd swerve states for advantage scope
    public double[] getDesierdStates(){    
      double[] desiredStates = {
          modules[0].getDesiredAngle().getRadians(),
          modules[0].getDesiredVelocity(),
          modules[1].getDesiredAngle().getRadians(),
          modules[1].getDesiredVelocity(),
          modules[2].getDesiredAngle().getRadians(),
          modules[2].getDesiredVelocity(),
          modules[3].getDesiredAngle().getRadians(),
          modules[3].getDesiredVelocity()
          };
      return desiredStates;
    }

    public Pose2d getPose() {
        return poseEstimator.getEstimatedPosition();
    }

    //resets all modules to absolute
    public void resetModulesToAbsolute() {
        for (SwerveModule mod : modules) {
            mod.resetToAbsolute();
        }
  }
    public void enableStateDeadband(boolean stateDeadBand) {
      for (int i = 0; i < 4; i++) {
          modules[i].enableStateDeadband(stateDeadBand);
      }
  }

    public PIDController getXController() {
      return xController;
  } 
        /**
     * Method to drive the robot using joystick info.
     *
     * @param xSpeed        speed of the robot in the x direction (forward) in m/s
     * @param ySpeed        speed of the robot in the y direction (sideways) in m/s
     * @param rot           angular rate of the robot in rad/s
     * @param fieldRelative whether the provided x and y speeds are relative to the field
     * @param isOpenLoop    whether to use velocity control for the drive motors
     */
    public void drive(double xSpeed, double ySpeed, double rot, boolean fieldRelative, boolean isOpenLoop) {
      setChassisSpeeds((
                               fieldRelative
                                       ? ChassisSpeeds.fromFieldRelativeSpeeds(xSpeed, ySpeed, rot, getYaw())
                                       : new ChassisSpeeds(xSpeed, ySpeed, rot)
                       ),
                       isOpenLoop
                      );
  }
  public void setChassisSpeeds(ChassisSpeeds chassisSpeeds, boolean isOpenLoop) {
    if (RobotBase.isSimulation()) {
        pigeon.getSimCollection().addHeading(
                +Units.radiansToDegrees(chassisSpeeds.omegaRadiansPerSecond * Constants.LoopTime));
    }
    SwerveModuleState[] swerveModuleStates = Constants.Swerve.Kinematics.toSwerveModuleStates(chassisSpeeds);
    setModuleStates(swerveModuleStates, isOpenLoop);
} 
      /**
     * Stops all swerve modules.
     */
    public void stop() {
      for (int i = 0; i < 4; i++) {
          modules[i].stop();
      }
  }
    public void driveHeading(double xSpeed, double ySpeed, double heading, boolean fieldRelative) {
      double rot = rotationController.calculate(getYaw().getRadians(), heading);
      //SmartDashboard.putNumber("Heading PID Output", rot);
      setChassisSpeeds((
                              fieldRelative
                                      ? ChassisSpeeds.fromFieldRelativeSpeeds(xSpeed, ySpeed, rot, getYaw())
                                      : new ChassisSpeeds(xSpeed, ySpeed, rot)
                      ),
                      false
                      );
  }

  public void setModuleStates(SwerveModuleState[] swerveModuleStates, boolean isOpenLoop) {
        SwerveDriveKinematics.desaturateWheelSpeeds(swerveModuleStates, Constants.Swerve.kMaxSpeed);
        for (int i = 0; i < 4; i++) {
            modules[i].setDesierdState(swerveModuleStates[i], isOpenLoop);
        }
    }

    public PIDController getYController() {
      return yController;
    }

    public PIDController getRotationController() {
      return rotationController;
    }

    //returns camera pose for advantage scope
    public double[] getCameraPose(){               
      double[] pose = new double[3];
      pose[0] = Constants.VisionConstants.APRILTAG_CAMERA_TO_ROBOT.getX();
      pose[1] = Constants.VisionConstants.APRILTAG_CAMERA_TO_ROBOT.getY();
      pose[2] = Constants.VisionConstants.APRILTAG_CAMERA_TO_ROBOT.getZ();

      return pose;
    }
    
    /**
    * Sets up the shuffleboard tab for the swerve modules.
    */
    private void setupModulesShuffleboard() {
      if (Constants.Telemetry) {

          moduleChooser.setDefaultOption("Front Left", modules[0]);
          moduleChooser.addOption("Front Right", modules[1]);
          moduleChooser.addOption("Back Left", modules[2]);
          moduleChooser.addOption("Back Right", modules[3]);

          setUpFeedforwardSavers();

          // inputs
          modulesTab.add("Module Chooser", moduleChooser);
          driveVelocityEntry = modulesTab.add("Set Drive Velocity", 0).getEntry();
          steerVelocityEntry = modulesTab.add("Set Steer Velocity", 0).getEntry();
          steerAngleEntry = modulesTab.add("Set Steer Angle", 0).getEntry();
          driveStaticFeedforwardEntry = modulesTab.add(
                  "Drive kS FF",
                  driveStaticFeedForwardSaver[moduleChooser.getSelected().getModuleIndex()]
                                                            ).getEntry();

          driveVelocityFeedforwardEntry = modulesTab.add(
                  "Drive kV FF",
                  driveVelFeedForwardSaver[moduleChooser.getSelected().getModuleIndex()]
                                                              ).getEntry();

          steerStaticFeedforwardEntry = modulesTab.add(
                  "Steer kS FF",
                  steerStaticFeedForwardSaver[moduleChooser.getSelected().getModuleIndex()]
                                                            ).getEntry();

          steerVelocityFeedforwardEntry = modulesTab.add(
                  "Steer kV FF",
                  steerVelFeedForwardSaver[moduleChooser.getSelected().getModuleIndex()]
                                                              ).getEntry();
        }
    }

    /**
     * Sets up feedforward savers.
     */
    private void setUpFeedforwardSavers() {
      driveStaticFeedForwardSaver = new Double[]{
              modules[0].getDriveFeedForwardKS(),
              modules[1].getDriveFeedForwardKS(),
              modules[2].getDriveFeedForwardKS(),
              modules[3].getDriveFeedForwardKS()
      };
      driveVelFeedForwardSaver = new Double[]{
              modules[0].getDriveFeedForwardKV(),
              modules[1].getDriveFeedForwardKV(),
              modules[2].getDriveFeedForwardKV(),
              modules[3].getDriveFeedForwardKV()
      };
      steerStaticFeedForwardSaver = new Double[]{
              modules[0].getSteerFeedForwardKS(),
              modules[1].getSteerFeedForwardKS(),
              modules[2].getSteerFeedForwardKS(),
              modules[3].getSteerFeedForwardKS()
      };
      steerVelFeedForwardSaver = new Double[]{
              modules[0].getSteerFeedForwardKV(),
              modules[1].getSteerFeedForwardKV(),
              modules[2].getSteerFeedForwardKV(),
              modules[3].getSteerFeedForwardKV()
      };
    }

    public Double[] getDriveStaticFeedforwardArray() {
        return driveStaticFeedForwardSaver;
    }

    public void setModuleStates(SwerveModuleState[] swerveModuleStates) {
      setModuleStates(swerveModuleStates, false);
  }


    public Double[] getDriveVelocityFeedforwardArray() {
        return driveVelFeedForwardSaver;
    }

    public Double[] getSteerStaticFeedforwardArray() {
        return steerStaticFeedForwardSaver;
    }

    public Double[] getSteerVelocityFeedforwardArray() {
        return steerVelFeedForwardSaver;
   }

    public Rotation2d getYaw() {
      return poseEstimator.getEstimatedPosition().getRotation();
    }

    public ChassisSpeeds getFieldRelativeChassisSpeeds() {
      return ChassisSpeeds.fromFieldRelativeSpeeds(
            getChassisSpeeds(),
            getPose().getRotation()
                                                );
    }

    public double getChassisSpeedsMagnitude() {
      return Math.hypot(
            getFieldRelativeChassisSpeeds().vxMetersPerSecond,
            getFieldRelativeChassisSpeeds().vyMetersPerSecond
                    );
    }

    public ChassisSpeeds getChassisSpeeds() {
      return Constants.Swerve.Kinematics.toChassisSpeeds(getModuleStates());
    }

    public SwerveModuleState[] getModuleStates() {
      SwerveModuleState[] states = new SwerveModuleState[4];
      for (SwerveModule mod : modules) {
          states[mod.getModuleIndex()] = mod.getState();
      }
      return states;
    }


    public Rotation2d getFieldRelativeHeading() {
      return Rotation2d.fromRadians(Math.atan2(
            getFieldRelativeChassisSpeeds().vxMetersPerSecond,
            getFieldRelativeChassisSpeeds().vyMetersPerSecond
                                            ));
    }

    public Rotation2d getPitch() {
      return Rotation2d.fromDegrees(pigeon.getPitch());
    }

    public Rotation2d getRoll() {
      return Rotation2d.fromDegrees(pigeon.getRoll());
    }

    public SwerveModulePosition[] getModulePositions() {
      SwerveModulePosition[] positions = new SwerveModulePosition[4];
      for (SwerveModule mod : modules) {
          positions[mod.getModuleIndex()] = mod.getPosition();
      }
      return positions;
    }


   /**
     * Returns the angular rate from the pigeon.
     *
     * @param id 0 for x, 1 for y, 2 for z
     * @return the rate in rads/s from the pigeon
     */
    public double getAngularRate(int id) {

      // uses pass by reference and edits reference to array
      double[] rawGyros = new double[3];
      pigeon.getRawGyro(rawGyros);

      // outputs in deg/s, so convert to rad/s
      return Units.degreesToRadians(rawGyros[id]);
    }

        /**
     * Resets the odometry to the given pose.
     *
     * @param pose the pose to reset to.
     */
    public void resetOdometry(Pose2d pose) {
      // NOTE: must use pigeon yaw for odometer!
      poseEstimator.resetPosition(Rotation2d.fromDegrees(pigeon.getYaw()), getModulePositions(), pose);
    }

    /**
     * Resets the yaw of the robot.
     *
     * @param rotation the new yaw angle as Rotation2d
     */
    public void setYaw(Rotation2d rotation) {
      resetOdometry(new Pose2d(getPose().getTranslation(), rotation));
    }

    public void resetYaw(){
      setYaw(new Rotation2d());
    }
    

    /**
     * Updates the field relative position of the robot.
     */
    public void updateOdometry() {
      // System.out.println(poseEstimator.getEstimatedPosition());
      // simGyro.updateGyroscope();
      // if(DriverStation.isAutonomousEnabled() && RobotBase.isSimulation())
      //     poseEstimator.update(Rotation2d.fromDegrees(simGyro.getYawDegrees()), getModulePositions());
      // else
          poseEstimator.update(Rotation2d.fromDegrees(pigeon.getYaw()), getModulePositions());

     if (RobotBase.isReal() && visionEnabled && Constants.VisionConstants.kEnabled) {

         // An array list of poses returned by different cameras
         ArrayList<EstimatedRobotPose> estimatedPoses = vision.getEstimatedPoses(poseEstimator.getEstimatedPosition());
         // The current position as a translation
         Translation2d currentEstimatedPoseTranslation = poseEstimator.getEstimatedPosition().getTranslation();
         for (EstimatedRobotPose estimatedPose : estimatedPoses) {
             // The position of the closest april tag as a translation
             Translation2d closestTagPoseTranslation = null;
             for (int j = 0; j < estimatedPose.targetsUsed.size(); j++) {
                 // The position of the current april tag
                 Pose3d currentTagPose = vision.getTagPose(estimatedPose.targetsUsed.get(j).getFiducialId());
                 // If it can't find the april tag's pose, don't run the rest of the for loop for this tag
                 if (currentTagPose == null) {
                     continue;
                 }
                 Translation2d currentTagPoseTranslation = currentTagPose.toPose2d().getTranslation();

                 // If the current april tag position is closer than the closest one, this makes makes it the closest
                 if (closestTagPoseTranslation == null || currentEstimatedPoseTranslation.getDistance(currentTagPoseTranslation) < currentEstimatedPoseTranslation.getDistance(closestTagPoseTranslation)) {
                     closestTagPoseTranslation = currentTagPoseTranslation;
                 }
             }

             double visionFactor = (currentEstimatedPoseTranslation.getDistance(closestTagPoseTranslation) * Constants.VisionConstants.kVisionPoseStdDevFactor);

             // Adds the vision measurement for this camera
             poseEstimator.addVisionMeasurement(
                     estimatedPose.estimatedPose.toPose2d(),
                     estimatedPose.timestampSeconds,
                     Constants.VisionConstants.kBaseVisionPoseStdDevs.plus(
                             visionFactor
                                                                )
                                               );
             if (Constants.logging) {
                 LogManager.addDouble("Vision/ClosestTag Distance",
                                      currentEstimatedPoseTranslation.getDistance(closestTagPoseTranslation)
                                     );
             }
         }
     }
    latestPositions = getModulePositions();
  }
  
  public void updateLogs() {

    loggerStep++;
    if (loggerStep < 4) return;
    loggerStep = 0;

    double[] pose = {
            getPose().getX(),
            getPose().getY(),
            getPose().getRotation().getRadians()
    };
    LogManager.addDoubleArray("Swerve/Pose2d", pose);

    double[] actualStates = {
            modules[0].getAngle().getRadians(),
            modules[0].getState().speedMetersPerSecond,
            modules[1].getAngle().getRadians(),
            modules[1].getState().speedMetersPerSecond,
            modules[2].getAngle().getRadians(),
            modules[2].getState().speedMetersPerSecond,
            modules[3].getAngle().getRadians(),
            modules[3].getState().speedMetersPerSecond
    };
    LogManager.addDoubleArray("Swerve/actual swerve states", actualStates);

    double[] desiredStates = {
            modules[0].getDesiredAngle().getRadians(),
            modules[0].getDesiredVelocity(),
            modules[1].getDesiredAngle().getRadians(),
            modules[1].getDesiredVelocity(),
            modules[2].getDesiredAngle().getRadians(),
            modules[2].getDesiredVelocity(),
            modules[3].getDesiredAngle().getRadians(),
            modules[3].getDesiredVelocity()
    };
    LogManager.addDoubleArray("Swerve/desired swerve states", desiredStates);

    double[] errorStates = {
      desiredStates[0] - actualStates[0],
      desiredStates[1] - actualStates[1],
      desiredStates[2] - actualStates[2],
      desiredStates[3] - actualStates[3],
      desiredStates[4] - actualStates[4],
      desiredStates[5] - actualStates[5],
      desiredStates[6] - actualStates[6],
      desiredStates[7] - actualStates[7]
    };
    LogManager.addDoubleArray("Swerve/error swerve states", errorStates);
}
    /**
     * Updates the drive module feedforward values on shuffleboard.
     */
    public void updateDriveModuleFeedforwardShuffleboard() {
      if (!Constants.Telemetry) return;
      // revert to previous saved feed forward data if changed
      if (prevModule != moduleChooser.getSelected()) {
          driveStaticFeedforwardEntry.setDouble(
                  driveStaticFeedForwardSaver[moduleChooser.getSelected().getModuleIndex()]
                                               );
          driveVelocityFeedforwardEntry.setDouble(
                  driveVelFeedForwardSaver[moduleChooser.getSelected().getModuleIndex()]
                                                 );
          prevModule = moduleChooser.getSelected();
      }

      // update saved feedforward data
      driveStaticFeedForwardSaver[moduleChooser.getSelected().getModuleIndex()] =
              driveStaticFeedforwardEntry.getDouble(0);
      driveVelFeedForwardSaver[moduleChooser.getSelected().getModuleIndex()] =
              driveVelocityFeedforwardEntry.getDouble(0);

      // to set all modules to same feedforward values if all
      // if (module.getSelected() == allModule) {
      //   for(int i = 0; i < 4; i++) {
      //     modules[i].setDriveFeedForwardValues(driveStaticFeedForwardSaver.get(module.getSelected()), driveVelFeedForwardSaver.get(module.getSelected()));
      //   }
      // }

      //set selected module
      moduleChooser.getSelected().setDriveFeedForwardValues(
              driveStaticFeedForwardSaver[moduleChooser.getSelected().getModuleIndex()],
              driveVelFeedForwardSaver[moduleChooser.getSelected().getModuleIndex()]
                                                           );
    }

  @Override
  public void periodic() {
    updateDriveModuleFeedforwardShuffleboard();
    updateOdometry();
    fieldDisplay.setRobotPose(getPose());
    if (Constants.logging) updateLogs();
  }
}
